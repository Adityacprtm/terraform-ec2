# Terraform EC2

- TLS auto generated
- EC2 instance in default VPC
- Docker ready

## How To

- Setup Env Var

  ```sh
  export AWS_ACCESS_KEY_ID="xxx"
  export AWS_SECRET_ACCESS_KEY="xxx"
  export AWS_DEFAULT_REGION="ap-southeast-1"
  ```

- Terraform command things

  ```sh
  terraform init
  terraform plan -out planFile
  terraform apply planFile

  # Get ssh_key value
  terraform output -raw ssh_key > ssh_key

  # check connection
  chmod 400 ssh_key
  ssh -i ssh_key ec2_user@ec2_host
  docker version
  ```

- Copy `ssh_key` wherever needed
