output "instance_ip" {
  value = aws_instance.aws_ec2.public_ip
}

output "ssh_key" {
  value     = tls_private_key.example.private_key_pem
  sensitive = true
}
